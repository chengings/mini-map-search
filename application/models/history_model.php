<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class History_model extends CI_Model {


	public function __construct()
	{
		$this->load->database();
	}

	/**
	* get city search result
	* @param $city a valid city name
	* @return string|null If found valid seach result return json string. Otherwise, null 
	*/
	public function get_city($city)
	{
		if (is_numeric($this->has_city($city))) {
			$saved_search_unixtime = $this->has_city($city);
			// different of current time and result time
			$diff_time = time() - $saved_search_unixtime;
			if ($diff_time < SECONDS_TO_UPDATE) {
				// okay, no need to update, get the old result
				$this->db->select('json_result');
				$this->db->from('history');
				$this->db->where('city', $city);
				$query = $this->db->get();
				if ($query->num_rows() > 0) {
					$result = $query->result();
					return $result[0]->json_result;
				}
			}
		}
		return NULL;
	}

	/**
	* add city name to history table
	* @param $city a valid city name
	* @param $json_result twitter search result in JSON format
	* @return boolean If insert properly, return true. Otherwise, false
	*/
	public function add_city($city, $json_result)
	{
		return $this->db->insert('history', array('city' => $city, 'timestamp' => time(), 'json_result' => $json_result));
	}

	/**
	* update city timestamp and search result to history table
	* @param $city a valid city name
	* @param $json_result twitter search result in JSON format
	* @return boolean If insert properly, return object. Otherwise, false
	*/
	public function update_city($city, $json_result)
	{
		$where = array('city' => $city);
		$data_update = array('timestamp' => time(), 'json_result' => $json_result);
		return $this->db->update('history', $data_update, $where);
	}

	/**
	* check city name in the history model
	* @param $city a valid city name
	* @return int|boolean If found, return unixtime. Otherwise, false
	*/
	public function has_city($city)
	{
		$this->db->select('timestamp');
		$this->db->from('history');
		$this->db->where('city', $city);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$result = $query->result();
			return $result[0]->timestamp;
		}
		return FALSE;
	}

	/**
	* get all cities from session
	* @return array|null $city If found, return an array of all cities. Otherwise, return null
	*/
	public function all_city()
	{
		$this->load->library('session');
		$valid_cities = $this->session->userdata(COOKIE_IN_HISTORY_CITIES);
		if ($valid_cities) {
			$cities_array = explode(',', $valid_cities);
			// remove unwanted empty value
			if (empty($cities_array[count($cities_array)])) {
				array_pop($cities_array);
			}
			return $cities_array;
		}
		return NULL;
	}
}