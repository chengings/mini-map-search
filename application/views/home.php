<?php 
  $attributes = array('id' => 'search-form');
  echo form_open('welcome/add_city', $attributes);
?>
<?php if (isset($city)) { echo '<h2 id="site_header" class="text-center upcase">Tweets about <span id="city_name">' . $city . '</span></h2>'; } ?>
<div class="row">
  <div class="small-10 small-centered column">
    <div class="flex-video" id="map-canvas"></div>
  </div>
</div>
<div class="row">
  <div class="small-10 small-centered column">
      <div class="row collapse">
        <div class="small-6 columns">
          <input id="address" type="text" name="address" placeholder="Enter the city" value="<?php if (isset($city)) { echo $city; } ?>" />
        </div>
        <div class="small-3 columns">
          <a href="javascript:void(0)" id="submit_add" class="button postfix upcase">Find</a>
        </div>
        <div class="small-3 columns">
          <a href="<?php echo base_url(); ?>index.php/search_history/" class="button postfix upcase">History</a>
        </div>
      </div>
  </div>
</div>
<?php echo form_close(); ?>