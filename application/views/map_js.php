<script type="text/javascript"
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAGhxZRKB_VoQB8gjXIdpRn0VJcOj-ho10&sensor=false">
</script>
<script>
  // IE 8 or lower does not have native trim function, so add the trim function by using string replace
  if (!String.prototype.trim) {
  	String.prototype.trim = function () {
    	return this.replace(/^\s+|\s+$/g, '');
    };
  }

  // add zero in number less than 10
  // 01 02 03 04 05 06 07 08 09
  function pad(number) {
  	if ( number < 10 ) {
  		return '0' + number;
  	}
    return number;
  }

  // format twitter date
  function parseTwitterDate(date_str) {
	var parsedDate = new Date(Date.parse(date_str));
	return parsedDate.getUTCFullYear() +
        '-' + pad( parsedDate.getUTCMonth() + 1 ) +
        '-' + pad( parsedDate.getUTCDate() ) +
        ' ' + pad( parsedDate.getUTCHours() ) +
        ':' + pad( parsedDate.getUTCMinutes() ) +
        ':' + pad( parsedDate.getUTCSeconds() );
  }

  // intialize google map
  function initialize() {
  	geocoder = new google.maps.Geocoder();
  	// Greenwich
	var latlng = new google.maps.LatLng(51.476878, 0.00033);
	var mapOptions = {
	    zoom: 12,
	    center: latlng
	}
	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
  }

  // set marker and info window
  function createMarkerInfoWindow(lat, lng, profile_img, txt) {
	var posLatLng = new google.maps.LatLng(lat, lng),
		marker = new google.maps.Marker({
			animation: google.maps.Animation.DROP,
			map: map,
			icon: profile_img,
            position: posLatLng,
            title: txt
    	}),
		infoWindow = new google.maps.InfoWindow({
			content: txt,
			zIndex: 99
		});
	google.maps.event.addListener(marker, 'click', function(){
		infoWindow.open(map, this);
	});
	// push marker to markers array
	markers[markers.length] = marker;
	return;
  }

  // perform search, set the datas to html and database 
  function codeAddress() {
  	var addressValue = document.getElementById('address').value;
  	addressValue = addressValue.trim();
  	if (addressValue) {
  		clearMarkers();
  		geocoder.geocode( { 'address': addressValue}, function(results, status) {
  			// inside this condition, we have got a valid city name
			if (status == google.maps.GeocoderStatus.OK) {
		      map.setCenter(results[0].geometry.location);
		      var url = '<?php echo base_url(); ?>index.php/get/tweet_by_city/' + addressValue + '/' + results[0].geometry.location.lat() + '/' + results[0].geometry.location.lng();
		      $.getJSON(url, function(dataJSON) {
		      	var statuses_array = dataJSON;
		      	if (statuses_array.length > 0) {
		      		for (var i = 0; i < statuses_array.length; i++) {
		      			var htmlTxtDetail = '<div><span>Tweet: ' + statuses_array[i].text + '</span><br/>' +
		      								  '<span>When: ' + parseTwitterDate(statuses_array[i].created_at) + '</span></div>';
						if (statuses_array[i].geo) {
							createMarkerInfoWindow(statuses_array[i].geo.coordinates[0],
								statuses_array[i].geo.coordinates[1],
								statuses_array[i].user.profile_image_url_https,
								htmlTxtDetail
								);
						}
		      		}
		      		// use jQuery to POST the from instead
					var formElem = document.getElementById('search-form');
				    if (formElem) {
				      	submitUrl = $(formElem).attr('action');
				      	var request = $.ajax({
				      						type: 'POST',
				      						dataType: 'json',
				      						url: submitUrl,
				      						data: { address: addressValue, json_result: JSON.stringify(dataJSON) }
				      					 });
				    }
				    // change city title name in real time
			  		$('#city_name').text(addressValue);
				}
		      });
		    }
		    else {
		      alert('Not found the city');
		    }
		});
  	}
  }
  // Sets the map on all markers in the array.
  function setAllMap(map) {
  	for (var i = 0; i < markers.length; i++) {
    	markers[i].setMap(map);
  	}
  }
  // Removes the markers from the map and destroy its array
  function clearMarkers() {
  	setAllMap(null);
  	markers = [];
  }
  // global variable
  var geocoder, map;
  var markers = [];

  $(document).ready(function() {
	  var formElem = document.getElementById('search-form'),
	  	  submitAddElem = document.getElementById('submit_add');
	  if (formElem) {
	  	$(formElem).on('submit', function(event) {
	  		codeAddress();
	  		// freeze browser form submitting
			event.preventDefault();
		});
	  }
	  if (submitAddElem) {
	  	$(submitAddElem).on('click', function(event) {
	  		codeAddress();
		});
	  }
	});

  /* from google maps doc, they suggest to load the map Onload
  * https://developers.google.com/maps/documentation/javascript/tutorial#LoadingMap
  */
  window.onload = function(){
  	initialize();
	codeAddress();
  }
  
  // center the map when resize window
  google.maps.event.addDomListener(window, "resize", function() {
  	var center = map.getCenter();
  	google.maps.event.trigger(map, "resize");
  	map.setCenter(center); 
  });
</script>