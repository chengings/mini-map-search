<h1 class='upcase'>Search History</h1>
<button class='upcase' id='back-button'>Back to the tweeets!</button>
<?php
$attrs = array('class' => 'inline-list upcase');
if (isset($all_cities) && !empty($all_cities)) {
	echo ul($all_cities, $attrs);
}
?>
<script>
$('#back-button').on('click', function(){
	history.back(-1);
});
</script>