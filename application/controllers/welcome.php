<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->model('history_model');
	}

	/**
	* index function of home cotroller, display the map and get address name
	* @return void
	*/

	public function index()
	{
		$data_view = array();
		if ($this->input->get_post('address')) {
			$data_view['city'] = $this->input->get_post('address');
		}
		$this->load->view('inc/header');
		$this->load->view('home', $data_view);
		$this->load->view('map_js');
		$this->load->view('inc/footer');
	}

	/**
	* add/update city and json result to session and database
	* to perform update action, need to compare search result last activity with current time
	* @return void
	*/

	public function add_city(){
		// filter cross site scripting hack 
		$search_city = $this->security->xss_clean($this->input->get_post('address'));
		$json_result = $this->input->get_post('json_result');
		// save only one format: lower string (english)
		$search_city = strtolower($search_city);
		if (!empty($search_city) && !empty($json_result)) {
			// Set city to session
			$all_cities = $this->history_model->all_city();
			$city = '';
			if (!empty($all_cities)) {
				$existing_cities = implode(',', $all_cities);
				// if not in city is not in the existing cookie, at the new city to the list
				if(!in_array($search_city, $all_cities)) {
					$city = $existing_cities . ',' . $search_city . ',';
				}
			}
			else{
				// no city in cookie, add the first one
				$city = $search_city . ',';
			}
			$this->session->set_userdata(COOKIE_IN_HISTORY_CITIES, $city);
			// Add/update city to db
			$is_hascity = $this->history_model->has_city($search_city);
			$need_update = FALSE;
			if(!empty($is_hascity) && is_numeric($is_hascity)) {
				$diff_time = time() - $is_hascity;
				// if city search result is older, set update flag
				if ($diff_time > SECONDS_TO_UPDATE) {
					$need_update = TRUE;
				}
			}
			// has city in db and search result expired
			if ($is_hascity && $need_update) {
				$this->history_model->update_city($search_city, $json_result);
			}
			// add new one
			elseif (!$is_hascity) {
				$this->history_model->add_city($search_city, $json_result);
			}
		}
		return;
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */