<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search_history extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->helper('html');
		$this->load->model('history_model');
	}


	/**
	* index function of search history cotroller, display the search result
	* @return void
	*/

	public function index()
	{
		// load site header
		$this->load->view('inc/header');
		$data_view = array();
		$all_cities = $this->history_model->all_city();
		if (!empty($all_cities)) {
			$this->load->helper('url');
			$all_cities_with_link = array();
			foreach ($all_cities as $city) {
				$all_cities_with_link[] = '<a href="' . base_url('index.php/?address=' . urlencode($city)) . '">' . $city .'</a>';
			}
			$data_view['all_cities'] = $all_cities_with_link;	
		}
		// load function html content
		$this->load->view('search_history', $data_view);
		// load site footer
		$this->load->view('inc/footer');
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */