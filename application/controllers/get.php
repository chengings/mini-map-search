<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Get extends CI_Controller {

	/**
     * search city radius
     * @access private
     */
	private $city_radius = '50km';
	
	/**
     * number of tweets in search result, maximum is 100
     * @access private
     * @link https://dev.twitter.com/docs/api/1.1/get/search/tweets#api-param-search_count
     */
	private $number_tweets = 55;

	public function __construct()
	{
		parent::__construct();
		// set twitter's tokens and secret
		$twitter_app_settings = array(
			'oauth_access_token' => "13418952-swmcCD5YzpnWaWzgTxiUNVQCLa4Q7llMu4UyexYGP",
			'oauth_access_token_secret' => "LwiZSaXZ1qKsOjH3QBc6NOg8nHSLS1oNe8T7qxoNPbvQm",
			'consumer_key' => "WkO8jwoEQGdp9oqxlsVl3xepU",
			'consumer_secret' => "oAx4KgCm1bmejwqjYiUFcrcZ7nHiXWKTTFv2621F57vfSiMNcX"
    	);
		$this->load->library('TwitterAPIExchange', $twitter_app_settings);
	}

	/**
	* Get geol twitter result, search by city name
	* @param $city a valid city anme
	* @param $la latitude of the city
	* @param $lo longitude of the city
	* @return json $response if found, return twitter resullt.
	*/

	public function tweet_by_city($city, $la, $lo)
	{
		if (empty($city) || empty($la) || empty($lo)) {
			throw new Exception('tweet_by_city parameters must have value');
		}
		$this->load->model('history_model');
 		$existing_result = $this->history_model->get_city(urldecode($city));
 		$output_json = NULL;
 		// check existing search result, use it if not expired
 		if (!empty($existing_result)) {
 			$output_json = $existing_result;
 		}
 		else {
 			// if system has no data or search result expired, get tweets from twitter api
 			$url = 'https://api.twitter.com/1.1/search/tweets.json';
			$getfield = '?q=' . $city . '&count=' . $this->number_tweets . '&geocode=' . $la . ',' . $lo . ',' . $this->city_radius;
			$requestMethod = 'GET';
			$response_json = $this->twitterapiexchange->setGetfield($getfield)->buildOauth($url, $requestMethod)->performRequest();
			$response = json_decode($response_json);
			// filter wanted datas
			if (isset($response->statuses) && is_array($response->statuses) && !empty($response->statuses)) {
				$filtered_array = array();
				foreach ($response->statuses as $key => $value) {
					$filtered_data = new stdClass();
					// get only tweets that have coordinate
					if (!empty($value->geo)) {
						$filtered_data->geo = $value->geo;
						$filtered_data->text = $value->text;
						$filtered_data->created_at = $value->created_at;
						$sub_obj = new stdClass();
						$sub_obj->profile_image_url_https =  $value->user->profile_image_url_https;
						$filtered_data->user = $sub_obj;
						$filtered_array[] = $filtered_data;
					}
				}
				$output_json = json_encode($filtered_array);
			}
 		}
 		// set content type to json
		$this->output->set_content_type('application/json; charset=utf-8')->set_output($output_json);
		return;
	}

}

/* End of file get.php */
/* Location: ./application/controllers/get.php */